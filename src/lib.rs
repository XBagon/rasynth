#[macro_use]
extern crate vst;

use noise::{NoiseFn, OpenSimplex};
use rand::random;
use std::f64::consts::PI;
use vst::{
    api::{Events, Supported},
    buffer::AudioBuffer,
    event::Event,
    plugin::{CanDo, Category, Info, Plugin},
};

struct Rasynth {
    sample_rate: f64,
    time: f64,
    simplex_noise: OpenSimplex,
    notes: [u8; std::u8::MAX as usize],
}

impl Default for Rasynth {
    fn default() -> Self {
        Self {
            sample_rate: 44100.0,
            time: 0.0,
            simplex_noise: Default::default(),
            notes: [0; std::u8::MAX as usize],
        }
    }
}

impl Plugin for Rasynth {
    fn get_info(&self) -> Info {
        Info {
            name: String::from("raynth"),
            unique_id: 07044070,
            inputs: 0,
            outputs: 2,
            category: Category::Synth,
            ..Default::default()
        }
    }

    fn process_events(&mut self, events: &Events) {
        for event in events.events() {
            match event {
                Event::Midi(ev) => {
                    match ev.data[0] {
                        // if note on, increment our counter
                        144 => self.notes[ev.data[1] as usize] = ev.data[2],

                        // if note off, decrement our counter
                        128 => self.notes[ev.data[1] as usize] = 0,
                        _ => (),
                    }
                    // if we cared about the pitch of the note, it's stored in `ev.data[1]`.
                }
                _ => (),
            }
        }
    }

    fn set_sample_rate(&mut self, rate: f32) {
        self.sample_rate = f64::from(rate);
    }

    fn process(&mut self, buffer: &mut AudioBuffer<f32>) {
        let samples = buffer.samples();
        let (_, mut outputs) = buffer.split();

        for sample_idx in 0..samples {
            let output_sample: f64 = self
                .notes
                .iter()
                .enumerate()
                .map(|(note, &velocity)| {
                    if velocity == 0 {
                        0.0
                    } else {
                        let pitch_freq = midi_pitch_to_freq(note as u8);
                        let pitched_simplex_noise =
                            self.simplex_noise.get([self.time * pitch_freq, 0.0]);
                        let sine = (self.time * pitch_freq * PI * 2.0).sin();
                        (pitched_simplex_noise * 0.9 + sine * 0.1) * (velocity as f64 / 255.0)
                    }
                })
                .sum();

            self.time += 1.0 / self.sample_rate;

            for output in outputs.into_iter() {
                output[sample_idx] = output_sample as f32;
            }
        }
    }

    fn can_do(&self, can_do: CanDo) -> Supported {
        match can_do {
            CanDo::ReceiveMidiEvent => Supported::Yes,
            _ => Supported::Maybe,
        }
    }
}

fn midi_pitch_to_freq(pitch: u8) -> f64 {
    const A4_PITCH: i8 = 69;
    const A4_FREQ: f64 = 440.0;

    // Midi notes can be 0-127
    ((f64::from(pitch as i8 - A4_PITCH)) / 12.).exp2() * A4_FREQ
}

plugin_main!(Rasynth);
